/*****************************************************
    Plugin:         Labrin Plugin Collection mini 0.1
    Author:         Muradov Sadig (js)
    Author URL:     http://sadig.muradov.org
    About plugin:   Beta version of Labrin Plugins collection
*****************************************************/

(function($){
    ///////////////////////////////////////////////////
    // UI - simple select
    $.fn.simpleSelect = function(settings){
        
       var defaultParametres = {
                tpl : '<span class="ui-labr-simpleselect">{firsttext}</span>'
           };
 
       var set= $.extend({}, defaultParametres, settings);
         
        return this.each(function(index, element){
            var elm = $(element), firstext = elm.find('option:selected').text(), classes = elm.attr('class'),
                span = $(set.tpl.replace('{firsttext}',firstext));
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            elm.wrap('<span class="select-wrps" style="display:inline-block;"/>');
            elm.before(span);
            var width = span.outerWidth(), height = span.outerHeight();
            elm.css({
                'width':width,
                'height':height,
                'margin-top': '-' + height + 'px'
            });
            elm.on('change', function(){
                span.text(elm.find('option:selected').text());
            });
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        });
    }
})(jQuery);
